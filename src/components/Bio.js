import React from "react";

// Import typefaces
import "typeface-montserrat";
import "typeface-merriweather";

import { rhythm } from "../utils/typography";

class Bio extends React.Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          marginBottom: rhythm(2.5),
        }}
      >
        <p>
          Written by <strong>Tianyi Wang</strong> who lives and works in Dublin
          Ireland building things.{" "}
          <a href="https://twitter.com/codeexploder">
            You should follow him on Twitter
          </a>
        </p>
      </div>
    );
  }
}

export default Bio;
